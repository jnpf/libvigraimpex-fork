#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS   = hardening=+bindnow

export DEB_LDFLAGS_MAINT_APPEND  = -Wl,--as-needed
DEB_CFLAGS_MAINT_APPEND   = -Wall -pipe
DEB_CXXFLAGS_MAINT_APPEND = -Wall -pipe
PYTHONVERSIONNUMBER = $(subst python,,$(shell py3versions -d))

AMCONFBUILDINDEP := $(shell if dh_listpackages | grep -q libvigraimpex-doc ; \
	then echo withindep ; \
	else echo archonly ; fi )
MAINBDIR = obj.$(DEB_TARGET_MULTIARCH)
MAINBDIRO = -O--builddirectory=$(MAINBDIR)

# support for Ubuntu development
# # Add -O2 at end to verride -O3 default setting
ifeq ($(DEB_HOST_ARCH),ppc64el)
	DEB_CFLAGS_MAINT_APPEND += -O2
	DEB_CXXFLAGS_MAINT_APPEND += -O2
endif

# fixing floating-point rounding errors on i386 CPUs, see #820429
ifneq (,$(filter $(DEB_HOST_ARCH_CPU), i386))
  DEB_CFLAGS_MAINT_APPEND += -ffloat-store
  DEB_CXXFLAGS_MAINT_APPEND += -ffloat-store
endif

# build with Boost.Thread on armel, see #814606
ifeq ($(DEB_HOST_ARCH),armel)
  CMAKE_EXTRAS := -DWITH_BOOST_THREAD=1
  # not set by VigraDetectThreading.cmake
  DEB_CXXFLAGS_MAINT_APPEND += -std=c++11
endif

ifneq (,$(filter $(DEB_BUILD_ARCH),mips mipsel))
  DEB_CXXFLAGS_MAINT_APPEND += --param ggc-min-expand=20
endif

export DEB_CFLAGS_MAINT_APPEND
export DEB_CXXFLAGS_MAINT_APPEND

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial/libhdf5.so),)
  export CMAKE_INCLUDE_PATH=/usr/include/hdf5/serial
  export CMAKE_LIBRARY_PATH=/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial
endif

override_dh_auto_configure:
	dh_auto_configure --verbose $(MAINBDIRO) -- \
		-DLIB_SUFFIX="/$(DEB_HOST_MULTIARCH)/" \
		-DWITH_OPENEXR=ON \
		-DPYTHON_EXECUTABLE=/usr/bin/python3 \
		-DPYTHON_INCLUDE_DIR=/usr/include/python$(PYTHONVERSIONNUMBER) \
		$(CMAKE_EXTRAS)
	find $(MAINBDIR)/vigranumpy/ -name 'link.txt' -exec sed -i \
		-e 's/ -lpython[0-9.]\+ / /g' \
		-e 's: /usr/[^ ]*/libpython[0-9.]\+\.so::' \
		-e '/-Wl,-soname,vigranumpycore/b' \
		-e 's/ -Wl,-soname,[^ ]\+ / /' {} +

override_dh_auto_build:
	@echo DEBUGMEMSIZE ; if which free ; then free ; fi
	dh_auto_build --verbose $(MAINBDIRO)
ifeq ($(filter archonly,$(AMCONFBUILDINDEP)),)
	$(MAKE) doc_cpp -C $(MAINBDIR)/
	ln -sf $(CURDIR)/vigranumpy/docsrc/c_api_replaces.txt \
		$(MAINBDIR)/vigranumpy/docsrc/
	$(MAKE) doc_python -C $(MAINBDIR)
endif

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	sed -i 's/exit\ 1/exit\ 0/' \
		$(MAINBDIR)/test/multiarray/run_test_multiarray_chunked.sh #812852
	sed -i 's/-O2/-O2 -O0/' \
		$(MAINBDIR)/test/math/CMakeFiles/test_math.dir/flags.make #817840
#	sed -i 's/-O2/-O2 -O1/' \
		$(MAINBDIR)/test/blockwisealgorithms/CMakeFiles/test_blockwiselabeling.dir/flags.make # https://github.com/ukoethe/vigra/issues/409
#	sed -i 's/-O2/-O2 -O1/' \
		$(MAINBDIR)/test/blockwisealgorithms/CMakeFiles/test_blockwisewatersheds.dir/flags.make # https://github.com/ukoethe/vigra/issues/409, #853513
	sed -i 's/-O2/-O2 -O1/' \
		$(MAINBDIR)/test/gridgraph/CMakeFiles/test_gridgraph.dir/flags.make #871556
	sed -i 's/-O2/-O2 -O0/' \
		$(MAINBDIR)/test/multiarray/CMakeFiles/test_multiarray.dir/flags.make #872037
	$(MAKE) -C $(MAINBDIR) check
	$(MAKE) -C $(MAINBDIR) vigranumpytest
else
	@echo 'Not running testsuite, "nocheck" in $$DEB_BUILD_OPTIONS'
endif

override_dh_auto_install:
	dh_auto_install $(MAINBDIRO)

override_dh_install:
	find debian/tmp -type d -name doctrees -exec rm -rf {} +
	rm -vf debian/tmp/usr/doc/vigra/LICENSE.txt
	dh_install $(MAINBDIRO)
ifeq ($(filter archonly,$(AMCONFBUILDINDEP)),)
	cd debian/libvigraimpex-doc/usr/share/doc/libvigraimpex-dev/ && \
		rm -f html/jquery.js && \
		sed -r -i -e \
		's!(<a href=")LICENSE.txt(">)!\1/usr/share/doc/libvigraimpex-doc/copyright\2!g' \
		html/index.html
endif

%:
	dh $@ --builddirectory=$(MAINBDIR)
